#!/bin/bash

log() {
  echo -e ">>> \e[33m$@\e[0m"
}

myexec() {
  if [[ $V -eq 1 ]]; then
    $@
  else
    $@ 2>&1 | tee /dev/tty2 >> /root/toe.log
  fi
}
