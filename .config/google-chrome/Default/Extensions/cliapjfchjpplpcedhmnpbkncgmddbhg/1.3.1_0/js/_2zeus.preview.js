var JQ = jQuery.noConflict();
var XHR;

_2ZeusPreview = {
    // Source: http://www.techfounder.net/2008/05/17/simple-javascript-cache/
    // TODO: Make caching work
    Cache: {
        stack: {},
        load: function(id) {
            return (typeof(_2ZeusPreview.Cache.stack[id]) != 'undefined') ?
                    _2ZeusPreview.Cache.stack[id] :
                    false;
        },
        save: function(data, id) {
            _2ZeusPreview.Cache.stack[id] = data;
        },
        remove: function(id) {
            if (typeof(_2ZeusPreview.Cache.stack[id]) != 'undefined') {
                delete _2ZeusPreview.Cache.stack[id];
            }
        }
    },

    init: function(doc) {
        this.doc = doc;
        this.opacity = 0.85;
        this.width = 220;
        this.dist = '20px';

        // Determine the position of the information window
        /*
        var vert = prefman.getCharPref('extensions._2zeus.preview.vertical');
        var vval = this.dist;
        var horiz = prefman.getCharPref('extensions._2zeus.preview.horizontal');
        var hval = this.dist;

        // If the user wants to have the information window centered, take the
        // necessary measures to make it happen
        if (vert == 'center') { vert = 'top'; vval = '50%'; }
        if (horiz == 'center') { horiz = 'left'; hval = '50%; margin-left: -' + (this.width / 2) + 'px'; }
        */
        var ver = 'top';
        var vval = this.dist;
        var horiz = 'right';
        var hval = this.dist;

        // Create a new DIV element and add it to the DOM
        var infoID = '2zeus-info-window';
        var infoWindow = this.doc.createElement('div');
        infoWindow.setAttribute('id', infoID);
        this.doc.body.appendChild(infoWindow);
        var IW = JQ(infoWindow);
        IW.css({
            opacity: this.opacity,
            position: 'fixed',
            top: vval,
            right: hval,
            width: this.width + 'px',
            border: '1px solid #fff',
            backgroundColor: '#333',
            color: '#fff',
            fontWeight: 'bold',
            fontSize: '12px',
            textAlign: 'left',
            padding: '10px',
            display: 'none'
        });
        IW.css('-webkit-border-radius', '10px');

        JQ('a[href^=http://2ze.us/]', this.doc).mouseover(function(evt) {
            /*
             * Handles the appearance of the information window.  If the user
             * has chosen to not display the preview windows, don't show any.
             */

            // Stop any animations in progress
            IW.stop();

            // See if the user wants the info window to show up underneath
            // the 2Zeus links
            /*
            var byMouse = prefman.getBoolPref('extensions._2zeus.preview.by_link');
            if (byMouse) {
                // find the position of the link in question
                var pos = JQ(this).position();

                // move the info window to show up below said link
                IW.css({
                    position: 'absolute',
                    top: pos.top + JQ(this).height(),
                    left: pos.left
                });
            }
            */

            // TODO: get this working!
            var json = _2ZeusPreview.Cache.load(this.href);

            if (json === false) {
                // Tell the user that we're fetching the information
                IW.html('<div style="text-align: center">Fetching URL info...</div>');
                _2ZeusPreview.fadeIn(IW);

                //.replace('2ze.us', 'localhost:8080')
                // Request the information
                XHR = JQ.ajax({
                    url: this.href + '/',
                    dataType: 'json',
                    success: function(json) {
                        // try caching the result and display the info
                        //_2ZeusPreview.Cache.save(json, this.href);
                        _2ZeusPreview.showInfo(IW, json);
                    },
                    error: function(xr, status, err) {
                        IW.html("We apologize, but something is borked here.  Please try again.");
                    }
                });
            } else {
                // try to display the cached information
                _2ZeusPreview.showInfo(IW, json);
            }
        });

        JQ('a[href^=http://2ze.us/]', this.doc).mouseout(function() {
            /*
             * Handle the hiding of the preview information window.  The delay
             * specified in the preferences dialog is used here.  First, we must
             * abort any outstanding AJAX requests so we don't get empty info
             * windows popping up as soon as the request completes.
             */
            XHR.abort();
            //var delay = prefman.getIntPref('extensions._2zeus.preview.delay');
            //if (delay < 0) { delay = 0; }
            var delay = 1000;

            JQ(infoWindow).fadeOut(delay, function() {
                JQ(this).html('');
            });
        });
    },

    fadeIn: function(elm) {
        // Fade the information window in, and make sure it always arrives at
        // the desired opacity level.  I'm not in the mood to make this delay a
        // preference :)
        elm.fadeIn(200);
        if (elm.css('opacity') < this.opacity) {
            elm.animate({opacity: this.opacity}, 200);
        }
    },

    showInfo: function(elm, json) {
        /*
         * Show the information about the URL in a nice little window.
         */
        var url = json.url;
        /*
        var title = url.title;

        // Trim down long page titles
        if (title.length > 50) title = title.substring(0, 47) + '&hellip;';
        */

        var html = '<div style="text-align: center; font-size: 110%; border-bottom: 1px solid #fff;">' + url.domain + "</div>";
        //html += "<div>Site: " + url.domain + "</div>";
        html += "<div>Shortcut: " + url.shortcut + "</div>";
        html += "<div>URL Compression: " + url.compression + "</div>";
        html += "<div>Hits: " + url.hits + "</div>";
        elm.html(html);
        _2ZeusPreview.fadeIn(elm);
    }
}

_2ZeusPreview.init(document);

