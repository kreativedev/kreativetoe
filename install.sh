#!/bin/bash -e

set -o pipefail
source functions.sh
source /tmp/config.sh

# detect partition information
PTAB_TYPE=`parted -s ${BLOCK_DEVICE} print | gawk '/Partition Table:/ {print $3}' || echo ''`

if [[ "${PTAB_TYPE}" == "gpt" ]]; then
  log "Detected GPT partition table"

  log "Wiping the partition table on ${BLOCK_DEVICE}"
  cat gdisk_wipe.cfg | myexec gdisk ${BLOCK_DEVICE}

  log "Creating a new partition table on ${BLOCK_DEVICE}"
  cat gdisk.cfg | myexec gdisk ${BLOCK_DEVICE}
else
  log "Wiping the partition table on ${BLOCK_DEVICE}"
  myexec dd if=/dev/zero of=${BLOCK_DEVICE} bs=512 count=1

  log "Creating a new partition table on ${BLOCK_DEVICE}"
  cat fdisk.cfg | myexec fdisk ${BLOCK_DEVICE}
fi

# TODO: respect USE_LVM

log "Cleaning up any old LVM info"
myexec pvremove -ff -y ${BLOCK_DEVICE}2

log "Creating new LVM information"
myexec pvcreate ${BLOCK_DEVICE}2
myexec vgcreate vg0 ${BLOCK_DEVICE}2
myexec lvcreate -C y -L 1G vg0 -n swap
myexec lvcreate -l +100%FREE vg0 -n root

log "Formatting boot partition with ${FILESYSTEM}"
myexec mkfs.${FILESYSTEM} ${BLOCK_DEVICE}1

log "Formatting root partition with ${FILESYSTEM}"
myexec mkfs.${FILESYSTEM} /dev/vg0/root

log "Formatting swap partition"
myexec mkswap /dev/vg0/swap

log "Mounting filesystems"
myexec mount /dev/vg0/root /mnt
myexec mkdir /mnt/boot
myexec mount ${BLOCK_DEVICE}1 /mnt/boot

log "Turning on swap"
myexec swapon /dev/vg0/swap

log "Updating pacman"
myexec pacman -Syy pacman --noconfirm

log "Finding fastest mirrors"
myexec pacman -Syy reflector --noconfirm
reflector -l 10 -f 10 -p http -c "United States" -n 5 --sort rate --save /etc/pacman.d/mirrorlist

log "Installing basic packages... this can take anywhere from 3 minutes to an hour or more depending on your Internet connection"
myexec pacstrap /mnt $(<packages.txt)

log "Setting up /etc/fstab"
genfstab -p /mnt | sed 's#dm-3#mapper/vg0-swap#' >> /mnt/etc/fstab

log "Configuring installed system..."
cp configure.sh /mnt/
cp functions.sh /mnt/
cp /tmp/config.sh /mnt/config.sh

arch-chroot /mnt ./configure.sh

rm /mnt/{configure,functions,config}.sh

log "Unmounting filesystems"
umount /mnt/{boot,}
swapoff /dev/vg0/swap
#reboot

echo "You should now be in a good state to reboot into your new Arch system."

# vim:set ts=2 sw=2 et:
