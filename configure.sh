#!/bin/bash -e

set -o pipefail
source functions.sh
source /config.sh

log "Setting hostname to '${MACHINE_NAME}'"
echo ${MACHINE_NAME} > /etc/hostname
sed -i "s/.*localhost.*/\0\t${MACHINE_NAME}/" /etc/hosts

log "Setting timezone to ${TZ_REGION}/${TZ_NAME}"
ln -s /usr/share/zoneinfo/${TZ_REGION}/${TZ_NAME} /etc/localtime

# TODO: make customizeable
log "Setting locale to en_US"
echo "LANG=en_US.UTF-8" > /etc/locale.conf
sed -i 's/^#\(en_US.*\)/\1/' /etc/locale.gen
myexec locale-gen

# TODO: respect USE_LVM
log "Adding LVM to initcpio hooks"
sed -i 's/^\(HOOKS=".*\)\(filesystems .*\)/\1lvm2 \2/' /etc/mkinitcpio.conf
myexec mkinitcpio -p linux

# TODO: make customizeable
log "Installing syslinux bootloader"
sed -i 's/sda3/vg0\/root/g' /boot/syslinux/syslinux.cfg
myexec /usr/sbin/syslinux-install_update -iam

if [[ ${INSTALL_CONFIGS} == [Yy] ]]; then
  log "Adding InstArch repository"
  cat >> /etc/pacman.conf <<EOT
  [instarch]
  SigLevel = Never
  Server = http://instarch.codekoala.com/\$arch/
EOT

  log "Configuring defaults for awesome programs..."
  myexec pacman -Syyf codekoala-configs adduser-defaults --noconfirm

  log "Setting up MOTD generator"
  myexec pacman -S magic-motd --noconfirm
  myexec ln -s /usr/lib/systemd/system/magic-motd.service /etc/systemd/system/multi-user.target.wants/magic-motd.service
  myexec ln -s /usr/lib/systemd/system/magic-motd-ip.service /etc/systemd/system/multi-user.target.wants/magic-motd-ip.service
fi

log "Setting zsh to default login shell"
myexec useradd -Ds /bin/zsh
myexec chsh -s /bin/zsh
cat > /etc/shells <<EOT
#
# /etc/shells
#

/bin/zsh
/bin/sh
/bin/bash

# End of file
EOT

if [[ ${INSTALL_WM} == [Yy] ]] && [[ "${WM}" != "" ]]; then
  log "Installing Window Manager ${WM}"

  # any additional packages that are useful for the chosen WM
  case ${WM} in
    openbox)
      ADTL="obkey obapps"
      ;;
    *)
      ADTL=""
      ;;
  esac

  myexec pacman -S xorg arandr ${WM} ${ADTL} --noconfirm
fi

if [[ ${SETUP_DHCP} == [Yy] ]]; then
  log "Setting up DHCP for next boot"
  myexec ln -s /usr/lib/systemd/system/dhcpcd.service /etc/systemd/system/multi-user.target.wants/dhcpcd.service
fi

if [[ ${SETUP_SSH} == [Yy] ]]; then
  log "Setting SSH to start at boot"
  myexec ln -s /usr/lib/systemd/system/sshd.service /etc/systemd/system/multi-user.target.wants/sshd.service
fi

# vim:et ts=2 sw=2 ai:
