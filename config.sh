#!/bin/bash
#
# TODO: locale
# TODO: bootloader

INTERACTIVE=${I:-y}
DEFAULT_USE_LVM=y
DEFAULT_SETUP_SSH=y
DEFAULT_SETUP_DHCP=y
DEFAULT_INSTALL_WM=n
DEFAULT_INSTALL_CONFIGS=n

DEFAULT_FILESYSTEM=ext4
DEFAULT_BLOCK_DEVICE=/dev/sda
DEFAULT_WM=openbox
DEFAULT_MACHINE_NAME=archvm
DEFAULT_TZ_REGION=America
DEFAULT_TZ_NAME=Denver


# INTERACTIVE MODE {{{
if [[ "${INTERACTIVE}" == [Yy] ]]; then
  # BLOCK DEVICE {{{
  BLOCK_DEVICES=$(awk '/[[:alpha:]]$/ {if (NR>1) {print "/dev/"$4}}' /proc/partitions | sort)

  echo "============ Detected block devices ============"
  PS3="Which device shall we install Arch on? "
  select BLOCK_DEVICE in ${BLOCK_DEVICES}; do
    break
  done
  # }}}

  # FILESYSTEM {{{
  SUPPORTED_FILESYSTEMS=$(awk '/^\t/ {print $1}' /proc/filesystems | sort)

  echo "======== Detected supported filesystems ========"
  PS3="What filesystem shall we use? "
  select FILESYSTEM in ${SUPPORTED_FILESYSTEMS}; do
    break
  done

  read -p "Use Logical Volume Management (LVM)? [Yn] " USE_LVM
  USE_LVM=${USE_LVM:-${DEFAULT_USE_LVM}}
  # }}}

  # WINDOW MANAGER {{{
  read -p "Install a window manager? [yN] " INSTALL_WM
  INSTALL_WM=${INSTALL_WM:-${DEFAULT_INSTALL_WM}}
  if [[ ${INSTALL_WM} == [Yy] ]]; then
    WM_OPTIONS="openbox kde gnome xfce"

    PS3="Which Window Manager would you like? "
    select WM in ${WM_OPTIONS}; do
      break
    done
  fi
  # }}}

  # MISC CONFIG {{{
  read -p "Hostname? [${DEFAULT_MACHINE_NAME}] " MACHINE_NAME
  MACHINE_NAME=${MACHINE_NAME:-${DEFAULT_MACHINE_NAME}}

  TZ_REGIONS=$(find /usr/share/zoneinfo/ -maxdepth 1 -type d -printf "%f\n" | sed 1d | sort)

  PS3="Timezone region? "
  select TZ_REGION in ${TZ_REGIONS}; do
    TZ_NAMES=$(find /usr/share/zoneinfo/${TZ_REGION} -maxdepth 1 -type f -printf "%f\n" | sed 1d | sort)

    PS3="Timezone name? "
    select TZ_NAME in ${TZ_NAMES}; do
      break
    done

    break
  done

  read -p "Launch SSH server at boot? [Yn] " SETUP_SSH
  SETUP_SSH=${SETUP_SSH:-${DEFAULT_SETUP_SSH}}

  read -p "Launch DHCPCD at boot? [Yn] " SETUP_DHCP
  SETUP_DHCP=${SETUP_DHCP:-${DEFAULT_SETUP_DHCP}}

  read -p "Install Josh's configurations? [yN] " INSTALL_CONFIGS
  INSTALL_CONFIGS=${INSTALL_CONFIGS:-${DEFAULT_INSTALL_CONFIGS}}
  # }}}
else
  # NON INTERACTIVE DEFAULTS {{{
  USE_LVM=${DEFAULT_USE_LVM}
  SETUP_SSH=${DEFAULT_SETUP_SSH}
  SETUP_DHCP=${DEFAULT_SETUP_DHCP}
  INSTALL_WM=${DEFAULT_INSTALL_WM}
  INSTALL_CONFIGS=${DEFAULT_INSTALL_CONFIGS}
  FILESYSTEM=${DEFAULT_FILESYSTEM}
  BLOCK_DEVICE=${DEFAULT_BLOCK_DEVICE}
  WM=${DEFAULT_WM}
  MACHINE_NAME=${DEFAULT_MACHINE_NAME}
  TZ_REGION=${DEFAULT_TZ_REGION}
  TZ_NAME=${DEFAULT_TZ_NAME}
  # }}}
fi
# }}}

# spit the configuration to file
cat > /tmp/config.sh <<EOT
#!/bin/bash

USE_LVM=${USE_LVM}
SETUP_SSH=${SETUP_SSH}
SETUP_DHCP=${SETUP_DHCP}
INSTALL_WM=${INSTALL_WM}
INSTALL_CONFIGS=${INSTALL_CONFIGS}

FILESYSTEM=${FILESYSTEM}
BLOCK_DEVICE=${BLOCK_DEVICE}
WM=${WM}
MACHINE_NAME=${MACHINE_NAME}
TZ_REGION=${TZ_REGION}
TZ_NAME=${TZ_NAME}
EOT

# vim:et ts=2 sw=2 fdm=marker:
