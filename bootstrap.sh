#!/bin/bash -e

set -o pipefail

EXTRACT_ROOT=/root/toe

# clean up
rm -rf ${EXTRACT_ROOT} > /dev/null 2>&1
umount -f /mnt/{boot,} > /dev/null 2>&1
swapoff -a /dev/vg0/swap > /dev/null 2>&1

# setup
mkdir -p ${EXTRACT_ROOT}

# get scripts
curl https://bitbucket.org/kreativedev/kreativetoe/get/master.tar.gz | tar zxf - --strip 1 -C ${EXTRACT_ROOT}

# put scripts into known directory
#find ${EXTRACT_ROOT} -type f -exec mv -i {} ${EXTRACT_ROOT} \;
#rm -rf ${EXTRACT_ROOT}/instarch*

# install
cd ${EXTRACT_ROOT}

cat > /tmp/config.sh <<EOT
#!/bin/bash

USE_LVM=y
SETUP_SSH=y
SETUP_DHCP=y
INSTALL_WM=n
INSTALL_CONFIGS=n

FILESYSTEM=ext4
BLOCK_DEVICE=/dev/sda
WM=openbox
MACHINE_NAME=archvm
TZ_REGION=America
TZ_NAME=Denver
EOT

# ./config.sh

./install.sh
